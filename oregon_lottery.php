<?php

Print
"<h3>Monitor Group Betting</h3>"; 
$conn = mysqli_connect("ix.cs.uoregon.edu", "guest", "guest", "p2p_betting", "3011")
or die('Error connecting to MySQL server.');
?>

<html>
<head>
  <title>Oregon Lottery</title>
  </head>
  
  <body bgcolor="white">
  
  <p>The Oregon Lottery is concerned that the social nature of a peer to peer betting platform makes an already addicting act like gambling, and making it even more so.</p>
  <p>To address these concerns, we have provided a functionality here to see the difference in money won/lost from users that are in groups, and those that are not</p>
  <p>The goal is to provide a 24/7 service for the Oregon Lottery to monitor if group betting leads to significant money lost</p>
  <hr>
  
  
<?php
  

$sql1 = "SELECT ROUND(AVG(pl.profit_loss)) AS in_group_pl FROM (SELECT profit_loss FROM account AS a JOIN in_group AS ig ON ig.user_id=a.user_id WHERE ig.user_id IS NOT NULL GROUP BY ig.user_id) AS pl;";

$result1 = mysqli_query($conn, $sql1)
or die(mysqli_error($conn));
while($pl1 = mysqli_fetch_array($result1, MYSQLI_BOTH)) {
	$in_group_pl = $pl1['in_group_pl'];
}
$sql2 = "SELECT ROUND(AVG(profit_loss)) AS not_in_group_pl FROM account AS a LEFT JOIN in_group AS ig ON ig.user_id=a.user_id WHERE ig.user_id IS NULL;";
$result2 = mysqli_query($conn, $sql2)
or die(mysqli_error($conn));
while($pl = mysqli_fetch_array($result2, MYSQLI_BOTH)) {
	$not_in_group_pl = $pl['not_in_group_pl'];
}

Print "<p>Average Profit/Loss of users who are in a group: ".$in_group_pl."</p>";
Print "<p>Average Profit/Loss of users who are in a group: ".$not_in_group_pl."</p>";

?>
<br>
<br>
<br>
<button onclick="window.location.href='homepage.html'">Click to go back to homepage</button>
 
</body>
</html>
